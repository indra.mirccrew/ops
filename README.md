1. git clone https://gitlab.com/indra.mirccrew/ops.git
2. docker-compose up -d --build


how to finish the task:
1. clone simple service repository to local drive
2. create docker compose file to deploy multi container, simple service, influxdb and traefik
3. add label to container service in docker compose so traefik can read the service container 
4. deploy service with docker compose up 

how to test the deployment:
1. curl http://localhost/myservice/health
2. curl http://localhost/influxdb

problem encountered:
1. traefik not directing request /myservice and /influxdb to appropriate service

how to solve the problem:
i need to add some option in traefik label,

1. pathprefix in routers
2. stripprefix in routers
3. stripprefix in middlewares
  
 

